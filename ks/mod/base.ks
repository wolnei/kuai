# Description:
# - Kuai Base Packages

%packages

#artwork
gimp
inkscape

#Cedilha
gtk2-immodules
im-chooser

#energy saver
tlp
tlp-rdw

#Fonts
abattis-cantarell-fonts
google-droid-sans-fonts
#google-droid-serif-fonts 

#LibreOffice
libreoffice-calc
libreoffice-impress
libreoffice-writer

#MTP support
gvfs-mtp
libmtp
simple-mtpfs

#Samba
libsmbclient

#Utils
foomatic-filters
gparted
joe
kamera
numlockx
ntfs-3g
ntfsprogs
ntpdate
p7zip
rdesktop
system-config-language
tigervnc
yum-plugin-fastestmirror
yum-plugin-remove-with-leaves

%end