# Common packages removed from comps

%packages

# save some space
-abrt*
-amarok*
-apper
-calligra-*
-dragon
-empathy
-ekiga
-evolution*
#-gdm
-gnome-bluetooth
-gnome-desktop*
-gnome-key*
-gnome-menus
-gnome-online*
-gnome-session
-kamoso
-kcolorchooser*
-kcoloredit*
-kolourpaint*
-kernel-debug
-*kget* 
-*krdc*
-*krfb*
-kdepim
-kdepim-libs
-kipi-plugins*
-kiconedit
-kruler*
-konversation
-marble*
-nano
-qt-devel*
-sendmail
-sudo
-xterm

# We won't use these Asian fonts
-lklug-fonts
-sil-abyssinica-fonts
-sil-padauk-fonts
-jomolhari-fonts
-lohit-*
-baekmuk-*
-vlgothic-fonts-*
-stix-fonts
-cjkuni-*
-thai-*
-smc-meera-fonts
-ipa-pgothic-fonts
-kacst-*
-khmeros-base-fonts
-paktype-*
-paratype-pt-sans-fonts
-sil-nuosu-fonts
-tabish-eeyek-fonts
-wqy*

# remove unnecessary input methods
-anthy
-kasumi
-libchewing

%end