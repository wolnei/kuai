%post --nochroot

rm -rf $INSTALL_ROOT/usr/share/autostart/konqy_preload.desktop
rm -rf $INSTALL_ROOT/usr/share/autostart/kgpg.desktop
rm -rf $INSTALL_ROOT/usr/share/bookmarks/*

cp -arf ../../config/applications/defaults.list $INSTALL_ROOT/usr/share/applications/
cp -arf ../../config/applications/mimeinfo.cache $INSTALL_ROOT/usr/share/applications/

cp -arf ../../config/kde/layout.js $INSTALL_ROOT/usr/share/kde4/apps/plasma/layout-templates/org.kde.plasma-desktop.defaultPanel/contents/
cp -arf ../../config/kde/kdeglobals $INSTALL_ROOT/usr/share/kde-settings/kde-profile/default/share/config/ 
cp -arf ../../config/kde/plasmarc $INSTALL_ROOT/usr/share/kde-settings/kde-profile/default/share/config/

cp -arf ../../config/kde/minimal/kdeglobals $INSTALL_ROOT/usr/share/kde-settings/kde-profile/minimal/share/config/

#file-roller submenu extract compress kde
#cp -arf ../../config/file-roller/*.desktop $INSTALL_ROOT/usr/share/kde4/services/ServiceMenus/

#extra repositories
cp -arf ../repos/*.repo $INSTALL_ROOT/etc/yum.repos.d/

cp -arf ../repo/msttcore-fonts-2.5-1.noarch.rpm $INSTALL_ROOT/tmp/

%end

%post

rpm -ivh /tmp/msttcore-fonts-2.5-1.noarch.rpm

%end