# Description:
# - Kuai Print Packages 

%packages

#Brother
#brscan
#brscan-skey
#dcpj140*

#Epson
#iscan*
#esci-interpreter-gt-s80

#hp
hplip-gui
libsane-hpaio

#Impressora PDF
#cups-pdf

#Scanner
xsane

%end