# Description:
# - Kuai Multimedia Packages

%packages

#Microsoft Fonts
#msttcore-fonts

#Rar files
unrar
libunrar

#video codecs
gstreamer-plugins-ugly
gstreamer-ffmpeg

#player
vlc
vlc-extras

%end