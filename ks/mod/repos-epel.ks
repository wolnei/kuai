# repos.ks
# Description:
# - Kuai Extras Repository

repo --name=adobe --baseurl=http://linuxdownload.adobe.com/linux/x86_64/
repo --name=epel --baseurl=http://mirror.globo.com/epel/7/x86_64/
repo --name=kuai --baseurl=file:///media/hd/Dados/Linux-ISSEM/git/kuai/ks/repo/
repo --name=nux --baseurl=http://li.nux.ro/download/nux/dextop/el7/x86_64/
#repo --name=rpmfusion-free --baseurl=http://download1.rpmfusion.org/free/el/updates/7/x86_64/
#repo --name=rpmfusion-nonfree --baseurl=http://download1.rpmfusion.org/nonfree/el/updates/7/x86_64/

%packages

adobe-release
epel-release
nux-dextop-release
#rpmfusion-free-release
#rpmfusion-nonfree-release

%end