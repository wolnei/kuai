# kuai.ks
# Description:
# - Kuai EPEL Livecd Spin with the K Desktop Environment (KDE)

lang pt_BR.UTF-8
keyboard br-abnt2
timezone America/Sao_Paulo

%include centos-7-live-kde.ks
%include mod/base.ks
%include mod/media.ks
%include mod/minimization-epel.ks
%include mod/net.ks
%include mod/post-epel.ks
%include mod/printscan.ks
%include mod/repos-epel.ks

%packages

#language
hunspell-pt
kde-l10n
kde-l10n-Brazil
libreoffice-langpack-pt-BR

#Login Manager
#lightdm-kde

#Rar gui fix
#file-roller

%end