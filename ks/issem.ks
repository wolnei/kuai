# ISSEM requirements

selinux --disabled
services --enabled=autofs,cups,NetworkManager,pcscd,sshd --disabled=avahi-daemon,chronyd,iscsi,iscsid,iscsiuio,libvirtd,lvm2-monitor,lvm2-lvmetad,mcelog,mdmonitor,network,postfix,spice-vdagentd,tcsd

%include kuai-epel.ks
%include issem-post.ks

%packages

#Positivo C610
kernel-devel
dkms-r8168

kdeartwork-wallpapers

#thunderbird
thunderbird
tnef-dolphin

#Certificado Digital
compat-libtiff3
gdbm-devel
libpng12
pcsc-lite
pcsc-lite-ccid
redhat-lsb

#remove 

-kgpg
-konversation
-ktorrent

#utilities
openssh-server
syslinux-extlinux
system-config-users

%end