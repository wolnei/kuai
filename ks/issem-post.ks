%post --nochroot

cp -arf ../../issem/anaconda/*.png $INSTALL_ROOT/usr/share/anaconda/pixmaps/
cp -arf ../../issem/applications/*.desktop $INSTALL_ROOT/usr/share/applications/
cp -ar ../../issem/kde/kickoffrc $INSTALL_ROOT/usr/share/kde-settings/kde-profile/default/share/config/
cp -arf ../../issem/pixmaps/*.png $INSTALL_ROOT/usr/share/pixmaps/

cp -arf ../../issem/configs/mozilla/all-redhat.js $INSTALL_ROOT/usr/lib64/thunderbird/defaults/pref/
cp -arf ../../issem/configs/registrymodifications.xcu $INSTALL_ROOT/usr/lib64/libreoffice/presets/
cp -arf ../../issem/configs/deployment.properties $INSTALL_ROOT/usr/share/icedtea-web/
cp -arf ../../issem/configs/mozilla/issem.js $INSTALL_ROOT/usr/lib64/firefox/defaults/preferences/
cp -arf ../../issem/configs/autofs/* $INSTALL_ROOT/etc/
cp -arf ../../issem/configs/cups/* $INSTALL_ROOT/etc/cups/
cp -arf ../../issem/configs/environment $INSTALL_ROOT/etc/
cp -arf ../../issem/configs/hosts $INSTALL_ROOT/etc/
cp -arf ../../issem/configs/hostname $INSTALL_ROOT/etc/
cp -arf ../../issem/configs/home-configs.sh $INSTALL_ROOT/home/
cp -arf ../../issem/configs/grub $INSTALL_ROOT/etc/default/
cp -arf ../../issem/configs/net.properties $INSTALL_ROOT/usr/lib/jvm/jre-1.7.0-openjdk/lib/
cp -arf ../../issem/configs/rc.local $INSTALL_ROOT/etc/rc.d/
cp -arf ../../issem/configs/root $INSTALL_ROOT/var/spool/cron/
cp -arf ../../issem/configs/smb.conf $INSTALL_ROOT/etc/samba/
cp -arf ../../issem/configs/sshd_config $INSTALL_ROOT/etc/ssh/
cp -arf ../../issem/configs/yum.conf $INSTALL_ROOT/etc/

cp -arf ../../issem/configs/firewall/firewalld.conf $INSTALL_ROOT/etc/firewalld/
cp -arf ../../issem/configs/firewall/work.xml $INSTALL_ROOT/etc/firewalld/zones/

cp -arf ../../issem/rpms/SafeSignIC3093-x86_64-RH6.rpm $INSTALL_ROOT/tmp/

mkdir -p $INSTALL_ROOT/smb/usuarios

%end

%post

rpm -ivh /tmp/SafeSignIC3093-x86_64-RH6.rpm --nodeps
ln -s /usr/lib64/libgdbm_compat.so.4 /usr/lib64/libgdbm.so.2

%end