#/bin/sh
libre=/home/$(whoami)/.config/libreoffice/4/user/registrymodifications.xcu
java=/home/$(whoami)/.config/libreoffice/4/user/
if [ ! -f "$libre" ]
then
    mkdir -p /home/$(whoami)/.config/libreoffice/4/user/
    cp /usr/lib64/libreoffice/presets/registrymodifications.xcu /home/$(whoami)/.config/libreoffice/4/user/
fi

if [ ! -f "$java" ]
then
    mkdir -p /home/$(whoami)/.config/icedtea-web/
    cp /usr/share/icedtea-web/deployment.properties /home/$(whoami)/.config/icedtea-web/
fi